#!/usr/bin/env python

from numpy import *
# make sure you are importing the gitlab verision of pyPLUTO.py
import pyPLUTO as pp
import pylab
import sys

# import solution from current dir
# e.g., 1 -> data.0001.dbl
fileno = int(sys.argv[1])
sol = pp.pload(fileno)

# extract the density at midplane (theta = pi/2)
rho = sol.rho[:,-1,:]

R, P = meshgrid(sol.x1, sol.x3)

X = R*cos(P)
Y = R*sin(P)

ax = pylab.subplot(111)
ax.set_aspect('equal')

pylab.pcolor(X, Y, rho.T, cmap=pylab.cm.jet_r, vmax=10)
pylab.colorbar()
pylab.show()
